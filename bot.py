#!/usr/bin/env python3.5

import asyncio
import discord
from discord import game
from discord.ext import commands
import praw
import time
import os
import random
import configparser

settings = configparser.ConfigParser()

if os.path.isfile("settings.ini") == False:
	if os.path.isfile("sample.settings.ini"):
		# Rename 'sample.settings.ini' to 'settings.ini'
		
		os.rename("sample.settings.ini", "settings.ini")
	
settings.read("settings.ini")

# Music bot variables
#settings["SOUND"]["directory"] = "/var/www/html/discord/sounds/"
sounds = {}
leave_after = set()
allow_queue = set()

# Chat bot variables
POST_TYPE = {}
POST_TYPE["media"] = {"IMAGE", "VIDEO"}
POST_TYPE["media_pref"] = {"IMAGE", "VIDEO"}

MEDIA_TYPES = {"jpg", "jpeg", "png", "gif", "gifv", "mp4"}

def load_sounds():
	global sounds
	old_sounds = sounds
	new_sounds = []
	removed_sounds = []
	sounds = {}

	for file in os.listdir(settings["SOUND"]["directory"]):
		if file.endswith(".mp3") or file.endswith(".wav"):
			sounds[file[:-4]] = file
			
			if file[:-4] not in old_sounds:
				new_sounds.append(file[:-4])
				
	for sound in old_sounds:
		if sound not in sounds:
			removed_sounds.append(sound)
				
	return new_sounds, removed_sounds

if not discord.opus.is_loaded():
	# the 'opus' library here is opus.dll on windows
	# or libopus.so on linux in the current directory
	# you should replace this with the location the
	# opus library is located in and with the proper filename.
	# note that on windows this DLL is automatically provided for you
	discord.opus.load_opus("opus")

class VoiceEntry:
	def __init__(self, ctx, player, title = "No Title"):
		self.ctx = ctx
		self.player = player
		self.title = title

	def __str__(self):
		return self.title

class VoiceState:
	def __init__(self, music, server):
		self.current = None
		self.voice = None
		self.leave_after = leave_after
		self.bot = music.bot
		self.music = music
		self.server = server
		self.play_next_song = asyncio.Event()
		self.songs = asyncio.Queue()
		self.skip_votes = set() # a set of user_ids that voted
		self.audio_player = self.bot.loop.create_task(self.audio_player_task())

	def is_playing(self):
		if self.voice is None or self.current is None:
			return False

		player = self.current.player
		return not player.is_done()

	@property
	def player(self):
		return self.current.player

	def skip(self):
		self.skip_votes.clear()
		if self.is_playing():
			self.player.stop()

	def toggle_next(self):
		self.bot.loop.call_soon_threadsafe(self.play_next_song.set)
		
	async def audio_player_task(self):
		while True:
			self.play_next_song.clear()
			self.current = await self.songs.get()
			self.current.player.start()
			await self.play_next_song.wait()
			
			# Remove current song.
			self.current = None
			
			# Leave the server if the option is set.
			if self.server.id in leave_after and self.songs.empty():
				await self.voice.disconnect()
				del self.music.voice_states[self.server.id]
				self.audio_player.cancel()
				break

class Music:
	"""Voice related commands.
	Works in multiple servers at once.
	"""
	def __init__(self, bot):
		self.bot = bot
		self.voice_states = {}

	def get_voice_state(self, server):
		state = self.voice_states.get(server.id)
		if state is None:
			state = VoiceState(self, server)
			self.voice_states[server.id] = state

		return state

	async def create_voice_client(self, channel):
		voice = await self.bot.join_voice_channel(channel)
		state = self.get_voice_state(channel.server)
		state.voice = voice

	def __unload(self):
		for state in self.voice_states.values():
			try:
				state.audio_player.cancel()
				if state.voice:
					self.bot.loop.create_task(state.voice.disconnect())
			except:
				pass

	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def join(self, ctx, *, channel : discord.Channel):
		"""Joins a voice channel."""
		try:
			await self.create_voice_client(channel)
		except discord.ClientException:
			await self.bot.say('Already in a voice channel...')
		except discord.InvalidArgument:
			await self.bot.say('This is not a voice channel...')
		else:
			await self.bot.say('Ready to play audio in ' + channel.name)

	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def summon(self, ctx):
		"""Summons the bot to join your voice channel."""
		summoned_channel = ctx.message.author.voice_channel
		if summoned_channel is None:
			await self.bot.say('You are not in a voice channel.')
			return False

		state = self.get_voice_state(ctx.message.server)
		if state.voice is None:
			state.voice = await self.bot.join_voice_channel(summoned_channel)
		else:
			await state.voice.move_to(summoned_channel)

		return True

	#@commands.command(pass_context=True, no_pm=True)
	async def play(self, ctx, song):
		# Plays a sound
		state = self.get_voice_state(ctx.message.server)

		if state.voice is None:# or 1:
			success = await ctx.invoke(self.summon)
			if not success:
				return
				
		# Checks if queueing is allowed.
		if state.current is not None and not ctx.message.server.id in allow_queue: return

		try:
			#player = await state.voice.create_ytdl_player(song, ytdl_options=opts, after=state.toggle_next)
			#player = await state.voice.create_ffmpeg_player(song)
			player = state.voice.create_ffmpeg_player(settings["SOUND"]["directory"] + song, after=state.toggle_next)#, use_avconv=True)
		except Exception as e:
			fmt = "An error occurred while processing this request: ```py\n{}: {}\n```"
			await self.bot.send_message(ctx.message.channel, fmt.format(type(e).__name__, e))
		else:
			player.volume = 0.6
			entry = VoiceEntry(ctx, player, song)
			#await self.bot.say("Playing " + str(entry))
			await state.songs.put(entry)
			
	@commands.command(pass_context=True, no_pm=True)
	async def yt(self, ctx, *, song : str):
		"""Plays a song.
		If there is a song currently in the queue, then it is
		queued until the next song is done playing.
		This command automatically searches as well from YouTube.
		The list of supported sites can be found here:
		https://rg3.github.io/youtube-dl/supportedsites.html
		"""
		state = self.get_voice_state(ctx.message.server)
		opts = {
			"default_search": "auto",
			"format": "bestaudio/worstaudio/worst",
			"quiet": True,
		}

		if state.voice is None:
			success = await ctx.invoke(self.summon)
			if not success:
				return
				
		# Checks if queueing is allowed.
		if state.current is not None and not ctx.message.server.id in allow_queue: return

		try:
			player = await state.voice.create_ytdl_player(song, ytdl_options=opts, after=state.toggle_next)
		except Exception as e:
			fmt = "An error occurred while processing this request: ```py\n{}: {}\n```"
			await self.bot.send_message(ctx.message.channel, fmt.format(type(e).__name__, e))
		else:
			player.volume = 0.05
			entry = VoiceEntry(ctx, player, "{} [{:02d}:{:02d}] ({})".format(player.title, int(player.duration)//60, int(player.duration)%60, player.download_url))
			await self.bot.say("{} [{:02d}:{:02d}]".format(player.title, int(player.duration)//60, int(player.duration)%60))
			await state.songs.put(entry)

	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def volume(self, ctx, value : int):
		"""Sets the volume of the currently playing song."""

		state = self.get_voice_state(ctx.message.server)
		if state.is_playing():
			player = state.player
			player.volume = value / 100
			await self.bot.say('Set the volume to {:.0%}'.format(player.volume))

	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def pause(self, ctx):
		"""Pauses the currently played song."""
		state = self.get_voice_state(ctx.message.server)
		if state.is_playing():
			player = state.player
			player.pause()

	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def resume(self, ctx):
		"""Resumes the currently played song."""
		state = self.get_voice_state(ctx.message.server)
		if state.is_playing():
			player = state.player
			player.resume()

	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def leave(self, ctx):
		"""Stops playing audio.
		This also clears the queue.
		"""
		await self._stop(ctx.message.server)
		
	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def stop(self, ctx):
		"""Stops playing audio and leaves the voice channel.
		This also clears the queue.
		"""
		# The skip command seems to work perfectly for this.
		state = self.get_voice_state(ctx.message.server)
		state.skip()
		
	async def _stop(self, server):
		
		state = self.get_voice_state(server)

		if state.is_playing():
			player = state.player
			player.stop()

		try:
			state.audio_player.cancel()
			
			del self.voice_states[server.id]
			await state.voice.disconnect()
				
		except:
			pass

	'''
	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def skip(self, ctx):
		"""Vote to skip a song. The song requester can automatically skip.
		3 skip votes are needed for the song to be skipped.
		"""

		state = self.get_voice_state(ctx.message.server)
		if not state.is_playing():
			await self.bot.say('Not playing any music right now...')
			return

		voter = ctx.message.author
		if voter == state.current.requester:
			await self.bot.say('Requester requested skipping song...')
			state.skip()
		elif voter.id not in state.skip_votes:
			state.skip_votes.add(voter.id)
			total_votes = len(state.skip_votes)
			if total_votes >= 3:
				await self.bot.say('Skip vote passed, skipping song...')
				state.skip()
			else:
				await self.bot.say('Skip vote added, currently at [{}/3]'.format(total_votes))
		else:
			await self.bot.say('You have already voted to skip this song.')
	'''

	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def playing(self, ctx):
		"""Shows info about the currently played song."""

		state = self.get_voice_state(ctx.message.server)
		if state.current is None:
			await self.bot.say("Not playing anything.")
		else:
			await self.bot.say("Now playing '{}'".format(state.current))
			
	
			
	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def reload(self, ctx):
	
		new_sounds, removed_sounds = load_sounds()
		
		if new_sounds == removed_sounds:
			#[] == [], both are empty.
			await self.bot.say("No new sounds loaded.")
			
		else:
			#Sounds added or removed.
			if new_sounds != []:
				await self.bot.say("New sounds loaded: " + ", ".join(new_sounds))
				
			if removed_sounds != []:
				await self.bot.say("Sounds removed: " + ", ".join(removed_sounds))
		
	@commands.command(pass_context=True, no_pm=True)
	async def list(self, ctx):
		# await self.bot.say(", ".join(sorted(sounds.keys())))
		await self.bot.say("http://75.us.to/discord")
			
	@commands.command(pass_context=True, no_pm=True)
	async def s(self, ctx, sound = None):
		if sound == None:
			# Play a random sound.
			sound = random.choice(list(sounds.keys()))
			
			while "_" in sound:
				sound = random.choice(list(sounds.keys()))
				
			await self.play(ctx, sounds[sound])
			
		elif sound in sounds:
			# Sound was found.
			await self.play(ctx, sounds[sound])
			
		else:
			# Sound is not found -- Try to see if it is a prefix.
			# Play random sound with prefix if found.
			soundlist = list(filter(lambda x: sound in x, list(sounds.keys())))
			
			if soundlist:
				await self.play(ctx, sounds[random.choice(soundlist)])
		
	@commands.command(pass_context=True, no_pm=True, hidden=True)
	async def test(self, ctx):
		print(self.voice_states)
		
	@commands.command(pass_context=True, no_pm=True)
	async def toggleleave(self, ctx):
	
		global leave_after
		
		if ctx.message.server.id in leave_after:
			await self.bot.say("I will stay after I speak.")
			leave_after.remove(ctx.message.server.id)
		else:
			await self.bot.say("I will leave after I speak.")
			leave_after.add(ctx.message.server.id)
			
	@commands.command(pass_context=True, no_pm=True)
	async def togglequeue(self, ctx):
	
		global allow_queue
		
		if ctx.message.server.id in allow_queue:
			await self.bot.say("Queueing is not allowed.")
			allow_queue.remove(ctx.message.server.id)
		else:
			await self.bot.say("Queueing is allowed.")
			allow_queue.add(ctx.message.server.id)
			
	@asyncio.coroutine
	async def on_voice_state_update(self, before, after):
		# If bot is the last one in the server, stop playing sound and leave.
		
		# If bot was moved to a channel by itself, it should disconnect.
		if after == bot.user and bot.voice_client_in(after.server) != None:
			if len(bot.voice_client_in(after.server).channel.voice_members) <= 1:
				await self._stop(after.server)
				
		botVoice = bot.voice_client_in(before.server)
		
		if before == bot.user or botVoice == None:
			# Ignore changes of bot's voice state or if bot is not connected to voice.
			return
		
		if len(botVoice.channel.voice_members) <= 1:
			await self._stop(before.server)
		
class Reddit:
	def __init__(self, bot, client_id = None, client_secret = None):
		self.bot = bot
		
		self.reddit = reddit = praw.Reddit(client_id=client_id, client_secret=client_secret, user_agent="Discord Bot - Made by /u/Steve8274")
		
	def yieldRandomFromSub(self, sub, limit = 5):
		for i in range(limit):
			yield self.reddit.subreddit(sub).random()
			
	def getPostFromSub(self, sub, method = "random", post_type = None, limit = 10):
	# post_type:	None = first it finds, media = only media, media_pref = media or just give last try.
	# method   : random, hot, new, top
		if method in {"random", "hot", "new", "top"}:
			if method == "random":
				submissions = self.yieldRandomFromSub(sub, 5)
			elif method == "hot":
				submissions = self.reddit.subreddit(sub).hot(limit=limit)
			elif method == "new":
				submissions = self.reddit.subreddit(sub).new(limit=limit)
			elif method == "top":
				submissions = self.reddit.subreddit(sub).top(limit=limit)
			else:
				return ("ERROR", None)
				
			for submission in submissions:
			# submission.domain
			# submission.title
			# submission.url
			# submission.post_hint
			
				if post_type == None:
					return (sub, submission)
				else:
					# post_hint doesn't always exist?
					# if submission.post_hint in POST_TYPE[post_type]:
					if submission.url.split(".")[-1] in MEDIA_TYPES:
						return (sub, submission)
				
		if post_type[-5:] == "_pref":
			# A type was preferred, but was not found.
			return (sub, submission)
			
		return (sub, None)
		
	def formatSubmission(self, submission, display_name = None):
		lines = []
		if display_name != None:
			lines.append("/r/" + display_name)
			
		lines.append(submission.title)
		lines.append(submission.url)
			
		return "\n".join(lines)
			
	@commands.command(pass_context=True, no_pm=True)
	async def image(self, ctx, *args):
		subreddit = args[0]
		if len(args) >= 2:
			method = args[1]
		else:
			method = "random"
			
		display_name, submission = self.getPostFromSub(subreddit, method=method, post_type="media_pref")
		await self.bot.say(self.formatSubmission(submission, display_name))
		
	@commands.command(pass_context=True, no_pm=True)
	async def random(self, ctx, *args):
		subreddit = self.reddit.random_subreddit().display_name
		
		if len(args) >= 1:
			method = args[0]
		else:
			method = "random"
			
		display_name, submission = self.getPostFromSub(subreddit, method=method, post_type="media_pref")
		await self.bot.say(self.formatSubmission(submission, display_name))
		
	@commands.command(pass_context=True, no_pm=True)
	async def nsfw(self, ctx, *args):
		subreddit = self.reddit.random_subreddit(True).display_name
		
		if len(args) >= 1:
			method = args[0]
		else:
			method = "random"
			
		display_name, submission = self.getPostFromSub(subreddit, method=method, post_type="media_pref")
		await self.bot.say(self.formatSubmission(submission, display_name))
		
	@commands.command(pass_context=True, no_pm=True)
	async def all(self, ctx, *args):
		subreddit = "all"
		
		if len(args) >= 1:
			method = args[0]
		else:
			method = "random"
			
		display_name, submission = self.getPostFromSub(subreddit, method=method, post_type="media_pref")
		await self.bot.say(self.formatSubmission(submission, display_name))
		
# BOT STARTS UP HERE
if settings["GENERAL"]["discord_token"] == "undefined":
	print("You must provide a Discord token to use this bot.")
	exit()

bot = commands.Bot(command_prefix=commands.when_mentioned_or("!"), description="Discord Bot")

bot.add_cog(Music(bot))

if settings["REDDIT"]["client_id"] != "undefined" and settings["REDDIT"]["client_secret"] != "undefined":
	bot.add_cog(Reddit(bot, settings["REDDIT"]["client_id"], settings["REDDIT"]["client_secret"]))
else:
	print("Could not load Reddit bot: 'client_id' or 'client_secret' were not defined.")

@bot.command(pass_context=True, no_pm=True)
async def restart(self):
	await self.bot.say("Restarting in 10 seconds.")
	print("Restarting . . .")
	await self.bot.logout()

@bot.event
async def on_message(message):
	# Do not reply to self.
	if message.author == bot.user:
		return
		
	if len(message.attachments) != 0 and bot.user in message.mentions:
		# If bot is mentioned and there is an attachment.
		
		for attach in message.attachments:
			# Check if each attachment is a new sound.
			print(attach)
	
	# If the function made it this far, parse for regular commands
	await bot.process_commands(message)

@bot.event
async def on_ready():
	print("Logged in as:\n{0} (ID: {0.id})".format(bot.user))
	load_sounds()
	await bot.change_presence(game = discord.Game(name = "!help"))
	#await bot.edit_profile(password = None, avatar = open("trump.jpg", "rb").read())

bot.run(settings["GENERAL"]["discord_token"])
